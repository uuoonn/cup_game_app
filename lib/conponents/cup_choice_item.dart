import 'package:flutter/material.dart';

class CupChoiceItem extends StatefulWidget {
  const CupChoiceItem({
    super.key, //부모가 필요한 걸 받아주고
    required this.pandon,
    required this.currentMoney,
  });

  final num pandon;
  final num currentMoney;

  @override
  State<CupChoiceItem> createState() => _CupChoiceItemState();
}

class _CupChoiceItemState extends State<CupChoiceItem> {
  bool _isOpen = false;
  String imgSrc = 'assets/cup.jpg';

  void _calculateStart() {
    // start 함수
    if (!_isOpen) {
      setState(() {
        _isOpen = true;
      });
    }
  }

  void _calculateImagSrc() {
    // 컵 열었을 때
    // 판돈이 현재 돈보다 적을때 : bomb.png
    // 판돈이 현재 돈과 같을 때 : coin.png
    // 판돈이 현재 돈보다 많을 때 : coins.png

    // 컵 안 열었을 때 : 무조건 cup.jpg

    String tempImageSrc = '';
    if (_isOpen) {
      if (widget.pandon < widget.currentMoney) {
        tempImageSrc = 'assets/gg.png';
      } else if (widget.pandon == widget.currentMoney) {
        tempImageSrc = 'assets/coin.png';
      } else {
        tempImageSrc = 'assets/coins.png';
      }
    } else {
      tempImageSrc = 'assets/cup.jpg';
    }
    setState(() {
      imgSrc = tempImageSrc;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _calculateStart();
        _calculateImagSrc();
      },
      child: Image.asset(
        imgSrc,
        width: 200,
        height: 200,


      ),
    );
  }
}
