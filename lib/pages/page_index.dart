import 'package:cup_game/conponents/cup_choice_item.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  bool _isStart = false; //시작했나 안했나 체크
  num pandon = 100;
  List<num> result = [0, 100, 200]; //참가 인원 수

  void _startGame() {
    setState(() {
      _isStart = true; //게임 시작하면 false에서 true로 바꾸고
      result.shuffle(); // 배열 셔플
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('야바위'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    if (_isStart) {
      return SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(50),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,

                children: [
                  CupChoiceItem(pandon: pandon, currentMoney: result[0]),
                  CupChoiceItem(pandon: pandon, currentMoney: result[1]),
                  CupChoiceItem(pandon: pandon, currentMoney: result[2]),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(100),
              child: OutlinedButton(
                onPressed: () {
                  setState(() {
                    _isStart = false;
                  });
                },
                child: const Text('종료'),
              ),
            )
          ],
        ),
      );
    } else {
      return SingleChildScrollView(
        child: Center(
          child: OutlinedButton(
            onPressed: () {
              _startGame();
            },
            child: const Text('시작'),
          ),
        ),
      );
    }
  }
}
